1. SELECT customerName FROM customers WHERE country LIKE "Philippines"


2. SELECT contactLastName, contactFirstName FROM customers where customerName LIKE "La Rochelle Gifts"

3. SELECT productName, MSRP FROM products WHERE productName LIKE "The Titanic"

4. SELECT firstName, lastName FROM employees WHERE email LIKE "jfirrelli@classicmodelcars.com"

5. SELECT customerName FROM customers WHERE state IS NULL

6. SELECT firstName,lastName,email from employees WHERE lastName LIKE "Patterson" AND firstName LIKE "Steve"

7. SELECT customerName, country, creditLimit FROM customers WHERE country NOT LIKE "USA" AND creditLimit > 3000

8. SELECT customerNumber From orders WHERE comments LIKE "%dhl%"

9. SELECT * FROM productlines WHERE textDecription LIKE "%state of the art%"

10. SELECT DISTINCT country FROM customers

11. SELECT DISTINCT status FROM orders

12. SELECT customerName, country FROM customers WHERE country LIKE "USA" OR country LIKE "France" OR country LIKE "Canada"

13. SELECT firstName,lastName,city FROM employees JOIN offices ON employees.officeCode =  offices.officeCode AND employees.officeCode = 5

14. SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber AND employees.employeeNumber = 1166

15. SELECT productName, customerName FROM products 
	JOIN orderdetails ON products.productCode =orderdetails.productCode 
	JOIN orders ON orderdetails.orderNumber = orders.orderNumber
	JOIN customers ON orders.customerNumber = customers.customerNumber
	AND customerName LIKE "%Baane Mini Imports%"

16. SELECT firstName, lastName,customerName,customers.country FROM employees JOIN customers ON customers.salesRepEmployeeNumber = employees.employeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode
	AND customers.country = offices.country

17. SELECT productName, quantityInStock FROM products WHERE productLine LIKE "Planes" AND quantityInStock < 1000

18. SELECT customerName FROM customers WHERE phone LIKE "%+81%"


